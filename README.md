## OnePlus6-user 10 QKQ1.190716.003 2103022249 release-keys
- Manufacturer: oneplus
- Platform: sdm845
- Codename: OnePlus6
- Brand: OnePlus
- Flavor: OnePlus6-user
- Release Version: 10
- Id: QKQ1.190716.003
- Incremental: 2103022249
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OnePlus/OnePlus6/OnePlus6:10/QKQ1.190716.003/2103022249:user/release-keys
- OTA version: 
- Branch: OnePlus6-user-10-QKQ1.190716.003-2103022249-release-keys
- Repo: oneplus_oneplus6_dump_10694


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
